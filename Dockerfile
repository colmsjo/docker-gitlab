FROM     centos:7
MAINTAINER Jonas Colmsjö "jonas@gizur.com"

RUN yum update -y
RUN yum install -y wget nano curl git unzip which tar gcc

RUN yum install -y python python-setuptools
RUN easy_install supervisor
ADD ./etc-supervisord.conf /etc/supervisord.conf
ADD ./etc-supervisor-conf.d-supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN mkdir -p /var/log/supervisor/

# gitlab prerequisites
RUN yum install -y curl openssh-server
RUN /usr/bin/ssh-keygen -A

# install gitlab
RUN curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | bash
RUN yum install -y gitlab-ce

RUN yum install -y rsyslog
ADD ./etc-rsyslog.conf /etc/rsyslog.conf

# backups are saved in S3
RUN wget https://github.com/s3tools/s3cmd/archive/master.zip
RUN unzip /master.zip
RUN cd /s3cmd-master; python setup.py install
RUN yum install -y python-dateutil
ADD ./s3cfg /.s3cfg

RUN yum install -y cronie
ADD ./batches.sh /

# Run backup job every hour
ADD ./backup.sh /
RUN echo '0 1 * * *  /bin/bash -c "/backup.sh"' > /mycron
RUN crontab /mycron

RUN mkdir /repos

EXPOSE 22
CMD ["supervisord"]

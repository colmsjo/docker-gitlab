gitlab container
================

1. `docker build --rm -t gitlab .`
or build fro scratch: `docker build --rm --no-cache=true -t gitlab .`

2. The docker build sometimes fail due to network errors etc. Repeat
`docker build --rm -t gitlab .` until the build succeeds.


3. Copy `env.list.template` to `env.list` and update. Do: `set -a; . env.list`

4. Start a container:

```
docker run -t -i -p 2022:22 --restart="on-failure:10" \
--link beservices:beservices --env-file=env.list -h gitlab --name gitlab \
-v `pwd`/repos:/repos \
gitlab /bin/bash -c "supervisord; export > /env; bash"
```

The `/repos` folder in the container is mounted to `./repos`. This folder
will persist when the container dies.

`beservices` is needed to send mails.


Test
----

Test that `ssh` works: `ssh [DOCKER HOST IP] -p 2022`. You should get a response but
will not be able to login.
